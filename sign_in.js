var basis = require('alpi_basis');
var speakeasy = require('speakeasy');

module.exports = class SignIn {
	/**
     * Creates an instance of Authentication.
     * 
     * @param {String} username 
     * @param {String} password 
     * @param {db} database
     */
	constructor(username, password, database) {
		this.username = username;
		this.password = basis.Hash.Sha256(password);
		this.database = database;
	}

	/**
     * Authenticates the user.
     * 
     * @param {Function} callback
     */
	CheckCreds(callback) {
		this.database.SelectPreparedRequest('SELECT username FROM Users WHERE username=? AND password=?', [this.username, this.password], (result) => {
			callback((result === false) ? false : result.rows.length > 0);
		});
	}

	/**
     * Checks if Two-Factor Authentication is enabled for the user and adds the TFA key to the user.
     * 
     * @param {Function} callback
     */
	TFAEnabled(callback) {
		this.database.SelectPreparedRequest('SELECT TFA FROM Users WHERE username=? AND password=?', [this.username, this.password], (result) => {
			if (result !== false && result.rows.length > 0) {
				this.TFA = result.rows[0].TFA;
				if (this.TFA === null) {
					callback(false);
				} else {
					callback(true);
				}
			} else {
				callback(false);
			}
		});
	}

	/**
     * Checks if the token provided is correct.
     * 
     * @param {String} token 
     * @param {Function} callback
     */
	CheckTFA(token, callback) {
		if (this.TFA === undefined) {
			module.exports.TFAEnabled((enabled) => {
				if (enabled === true) {
					callback(speakeasy.totp.verify({
						secret: this.TFA,
						encoding: 'base32',
						token: token
					}));
				} else {
					callback(false);
				}
			});
		}else{
			if (this.TFA !== null) {
				callback(speakeasy.totp.verify({
					secret: this.TFA,
					encoding: 'base32',
					token: token
				}));
			} else {
				callback(false);
			}
		}
	}

	/**
     * Creates the session token.
     * 
     * @param {String} userIP
     * @param {Number} expiresIn In ms
     */
	CreateSessionToken(userIP, expiresIn, callback) {
		CreateSessionsTable(this.database, (result) => {
			if (result === true) {
				this.token = basis.Hash.Sha256((Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)));
				this.database.QueryPreparedRequest('INSERT INTO Sessions VALUES(?, ?, ?, NOW() + INTERVAL ? MICROSECOND)', [this.token, this.username, userIP, expiresIn * 1000], (result)=>{
					callback((result !== false) ? this.token : false);
				});
			} else {
				callback(false);
			}
		});
		setTimeout(()=>{
			SignIn.RevokeInvalidSessionToken(this.database, ()=>{ });
		}, 500);
	}

	/**
	 * Refresh the session token.
	 * 
	 * @param {String} token 
	 * @param {String} userIp 
	 * @param {db} expiresIn 
	 * @param {Function} callback 
	 */
	static RefreshSessionToken(token, userIp, dbConnection, expiresIn, callback){
		SignIn.VerifySignInToken(token, userIp, dbConnection, (result)=>{
			if(result === true){
				dbConnection.QueryPreparedRequest('UPDATE Sessions SET expirationDate=NOW() + INTERVAL ? MICROSECOND WHERE token=? AND userIP=?', [expiresIn * 1000, token, userIp], (result)=>{
					callback(result !== false);
				});
			}else{
				callback(false);
			}
		});
	}

	/**
     * Checks the session token.
     * 
     * @param {String} token 
     * @param {String} userIP
     * @param {db} dbConnection 
     * @param {Function} callback 
     */
	static VerifySignInToken(token, userIP, dbConnection, callback) {
		dbConnection.SelectPreparedRequest('SELECT * FROM Sessions WHERE token=?', [token], (result) => {
			if (result !== false && result.rows.length > 0 && typeof result.rows[0] !== 'undefined') {
				if (result.rows[0].ip == userIP) {
					if (result.rows[0].expirationDate >= (new Date())) {
						callback(true);
					} else {
						callback(false); //"expiredToken"
					}
				} else {
					callback(false); //"wrongIP"
				}
			} else {
				callback(false); //"wrongToken"
			}
		});
	}

	/**
	 * Revoke the session token.
	 * 
	 * @param {String} token 
	 * @param {db} dbConnection 
	 * @param {Function} callback 
	 */
	static RevokeSessionToken(token, dbConnection, callback){
		dbConnection.QueryPreparedRequest('DELETE FROM Sessions WHERE token=?', [token], (result)=>{
			callback(result !== false);
		});
	}

	/**
	 * Revoke all invalid session tokens.
	 * 
	 * @param {db} dbConnection 
	 * @param {Function} callback 
	 */
	static RevokeInvalidSessionToken(dbConnection, callback){
		dbConnection.QueryRequest('DELETE FROM Sessions WHERE expirationDate<NOW()', (result)=>{
			callback(result !== false);
		});
	}
};

/**
 * Creates the table Sessions if it doesn't exists.
 * 
 * @param {db} dbConnection 
 * @param {Function} callback
 */
function CreateSessionsTable(dbConnection, callback) {

	dbConnection.QueryRequest('SELECT 1 FROM Sessions LIMIT 1;', function (result) {
		if (result === false) {
			dbConnection.QueryRequest(
				`CREATE TABLE \`Sessions\` (
					\`token\` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
					\`username\` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
					\`ip\` varchar(39) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					\`expirationDate\` datetime NOT NULL,
					PRIMARY KEY (\`token\`),
					KEY \`username\` (\`username\`),
					CONSTRAINT \`Sessions_ibfk_1\` FOREIGN KEY (\`username\`) REFERENCES \`Users\` (\`username\`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci`,(result)=>{
					callback(result !== false);
				});
		} else {
			//The table Session already exists
			callback(true);
		}
	});
}
