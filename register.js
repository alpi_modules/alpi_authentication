var basis = require('alpi_basis');
var speakeasy = require('speakeasy');
var QRCode = require('qrcode');

module.exports = class AccountCreation {
	/**
     * Creates an instance of AccountCreation.
     * 
     * @param {String} username 
     * @param {String} password 
     * @param {Boolean} TFA 
     * @param {db} database 
     */
	constructor(username, password, TFA, database) {
		this.username = username;
		this.password = basis.Hash.Sha256(password);
		this.TFA = TFA;
		this.database = database;
	}

	/**
     * Adds this user to the database.
     * 
     * @param {Function} callback
     */
	Create(callback) {
		CreateUsersTable(this.database, (exists) => {
			if (exists === true) {
				if (this.TFA === true) {
					this.secret = speakeasy.generateSecret({
						length: 20
					});
					this.database.QueryPreparedRequest('INSERT INTO Users VALUES(?, ?, ?)', [this.username, this.password, this.secret.base32], (result) => {
						callback(typeof result !== 'boolean');
					});
				} else {
					this.database.QueryPreparedRequest('INSERT INTO Users VALUES(?, ? , NULL)', [this.username, this.password], (result) => {
						callback(typeof result !== 'boolean');
					});
				}
			} else {
				callback(false);
			}
		});
	}

	/**
     * Generates a QRCode for Two-Factor Authentication.
     * 
     * @param {Function} callback 
     */
	GetQR(callback) {
		if (this.secret !== undefined) {
			QRCode.toDataURL(this.secret.otpauth_url, (err, image_data) => {
				callback(image_data);
			});
		} else {
			callback(false);
		}
	}

	/**
     * Retrieves the Two-Factor Authentication Secret.
     * 
     * @returns {String|Boolean}
     */
	GetSecret() {
		return (this.secret !== undefined) ? this.secret.base32 : false;
	}
};

/**
 * Creates the Users table if it doesn't exists.
 * 
 * @param {db} dbConnection 
 * @param {Function} callback 
 */
function CreateUsersTable(dbConnection, callback) {
	dbConnection.QueryRequest('SELECT 1 FROM Users LIMIT 1;', (result) => {
		if (result === false) {
			dbConnection.QueryRequest(
				`CREATE TABLE \`Users\` (
					\`username\` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
					\`password\` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
					\`TFA\` varchar(34) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
					PRIMARY KEY (\`username\`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci`, (result)=>{
					callback(result !== false);
				});
		} else {
			callback(true);
		}
	});
}
